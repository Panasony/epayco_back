# Garage APP
ePayco Back is a REST API of a system that must be able to register a client, load money to the wallet, make a purchase with a confirmation code and check the balance of the wallet.

## Table of Contents

* [Technical Support or Questions](#technical-support-or-questions)
* [Documentation](#docs)
* [Licensing](#licensing)

## Terminal Commands

1. Install NodeJs from [NodeJs Official Page](https://nodejs.org/en).
2. Create a database and configure credentials in ```.config/config.js``` file.
3. Open Terminal
4. Go to your file project
5. Run in terminal: ```npm install```
6. Run `npm start` for a dev server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.

## Technical Support or Questions

If you have questions or need help integrating the app please [contact me](https://github.com/Panasony19) instead of opening an issue.

## Licensing

- Copyright 2018 Joel Corona (https://github.com/Panasony19)

- Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-angular2/blob/master/LICENSE.md)
