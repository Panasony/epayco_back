var express = require('express');
var router = express.Router();
const db = require("../models/index");
const nodemailer = require("nodemailer");

/**
 * Generate unique token
 * @param {*} length length of the token to generate
 */
const uniqueToken = (length) => {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789$._-";

  for (var i = 0; i < length; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

/**
 * Transport to send mail
 */
const transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: 'panasony.ing.uc@gmail.com',
    pass: 'ansonytelecom'
  }
});

/**
 * Template to send email
 * @param {*} user user to send
 */
const sendEmail = async (user) => {
  const emailTemplate = {
    subject: "Validate Payment",
    html: `
        <p>Hello ${user.name},</p>
        <p>Validate your payment.</p>
        <p>Your access token is: <b>${user.token}</p>
        <p>to validate please click below:</p>
        <a href=${user.url}>${user.url}</a>`,
    to: user.email,
    from: 'ansony@email.com',
  };

  await transporter.sendMail(emailTemplate);
}


/**
 * Recharge wallet
 */
router.post('/recharge/:dni/phone/:phone', async (req, res, next) => {

  try {

    const { dni, phone } = req.params;

    const data = {
      amount: req.body.amount
    };

    if (data.amount <= 0) {
      return res.status(400).send({
        message: 'An error!, amount must be greater than zero'
      });
    }

    const user = await db.Users.findOne(
      {
        where: {
          document: dni,
          telephone: phone
        }
      });

    if (!user) {
      console.log('Not found!');
      return res.status(404).send({
        message: 'User not found!'
      });
    }

    await db.Wallets.increment({ amount: +data.amount }, {
      where: {
        user_id: user.id
      }
    });

    res.send({message: 'Amount updated'});
  } catch (error) {
    console.log(error)
    return res.status(500).send({
      message: 'An error recharge this user!'
    });
  }
});

/**
 * Gerante token to discount of wallet
 */
router.post('/generate-token/:id', async (req, res, next) => {
  try {

    const user = await db.Users.findByPk(req.params.id, { raw: true });

    if (!user) {
      console.log('Not found!');
      return res.status(404).send({
        message: 'User not found!'
      });
    }

    const wallet = await db.Wallets.findOne({
      where: {
        user_id: req.params.id
      },
      raw: true
    });

    if (!wallet) {
      console.log('Not found!');
      return res.status(404).send({
        message: 'Wallet not found!'
      });
    }

    if (wallet.amount < +req.body.amount) {
      return res.status(404).send({
        message: 'cannot be discounted, insufficient wallet amount!'
      });
    }

    const token = uniqueToken(6);

    await db.Wallets.update({ last_token: token }, {
      where: {
        user_id: user.id
      }
    });

    const url = `http://localhost:3000/wallets/verify-payment/${user.id}/token/${token}/amount/${req.body.amount}`

    sendEmail({ ...user, token, url });

    return res.status(200).send({
      message: 'Email send it successfully'
    })

  } catch (error) {
    console.log(error)
    return res.status(400).send({
      message: 'An error verify payment!'
    });
  }
})

/**
 * Verify payment
 */
router.get('/verify-payment/:id/token/:token/amount/:amount', async (req, res) => {

  try {
    const { id, token, amount } = req.params;

    const data = {
      amount
    };

    const wallet = await db.Wallets.findOne({
      where: {
        user_id: id,
        last_token: token
      },
      raw: true
    });

    if (!wallet) {
      console.log('Not found!');
      return res.status(404).send({
        message: 'Wallet not found!'
      });
    }

    await db.Wallets.decrement({ amount: data.amount }, {
      where: {
        id: wallet.id
      }
    })

    return res.send({
      message: 'Discount successfully'
    })

  } catch (error) {
    console.log(error)
    return res.status(500).send({
      message: 'An error recharge this user!'
    });
  }
})

/**
 * Get amount available by user
 */
router.get('/status/:dni/phone/:phone', async (req, res) => {
  try {

    const { dni, phone } = req.params;

    const user = await db.Users.findOne({
      where: {
        document: dni,
        telephone: phone
      },
      raw: true
    });

    if (!user) {
      console.log('Not found!');
      return res.status(404).send({
        message: 'User not found!'
      });
    };

    const wallet = await db.Wallets.findOne({
      where: {
        user_id: user.id
      }
    });

    return res.send({
      amount: wallet.amount,
      message: 'Amount available'
    });

  } catch (error) {
    console.log(error)
    return res.status(400).send({
      message: 'An error listing status!'
    });
  }
})

module.exports = router;
