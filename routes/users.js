var express = require('express');
var router = express.Router();
const db = require("../models/index");
const Joi = require("joi");

/**
 * Validate all fields in to create an user
 * @param {*} user Data to validate
 */
const validateUser = (user) => {
  const schema = Joi.object({
    document: Joi.number().min(3).required(),
    telephone: Joi.string().min(5).required(),
    name: Joi.string().min(3).required(),
    email: Joi.string()
      .max(255)
      .email()
      .required()
  });

  return schema.validateAsync(user);
}

router.get('/', async (req, res, next) => {
  try {
    const users = await db.Users.findAll({
      raw: true
    });

    return res.send({
      message: 'all user listed successfully',
      users
    });
  } catch (error) {
    console.log(error)
    return res.status(500).send({
      message: 'An error listing users!'
    });
  }
})

router.get('/:id', async (req, res, next) => {
  try {

    const { id } = req.params;

    const user = await db.Users.findByPk(id);

    return res.send({
      message: 'user listed successfully',
      user
    });
  } catch (error) {
    console.log(error)
    return res.status(500).send({
      message: 'An error listing the user!'
    });
  }
})

/**
 * Register user
 */
router.post('/', async (req, res, next) => {

  try {

    // validate data
    await validateUser(req.body);

    const data = {
      name: req.body.name,
      email: req.body.email,
      document: req.body.document,
      telephone: req.body.telephone
    };

    const userCreated = db.Users.build(data);

    await userCreated.save();

    // create waller
    const wallet = db.Wallets.build({
      user_id: userCreated.id,
      amount: 0
    });

    await wallet.save();

    res.send(userCreated);
  } catch (error) {
    console.log(error)
    return res.status(500).send({
      message: 'An error register this user!'
    });
  }
});

module.exports = router;
