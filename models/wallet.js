module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "Wallets",
    {
      amount: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
      last_token: DataTypes.STRING,
    },
    {
      tableName: 'Wallets',
      freezeTableName: true,
      timestamps: true,
      updatedAt: 'updated_at',
      createdAt: 'created_at',
    }
  );

  return User;
};
