module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "Users",
    {
      email: DataTypes.STRING,
      document: DataTypes.STRING,
      name: DataTypes.STRING,
      telephone: DataTypes.STRING
    },
    {
      tableName: 'Users',
      freezeTableName: true,
      timestamps: true,
      updatedAt: 'updated_at',
      createdAt: 'created_at',
    }
  );

  return User;
};
